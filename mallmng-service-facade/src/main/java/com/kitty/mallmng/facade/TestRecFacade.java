package com.kitty.mallmng.facade;

import com.kitty.mallmng.facade.vo.TestReq;
import com.kitty.mallmng.facade.vo.TestResp;

public interface TestRecFacade {
    TestResp testRpc(TestReq req);
}
