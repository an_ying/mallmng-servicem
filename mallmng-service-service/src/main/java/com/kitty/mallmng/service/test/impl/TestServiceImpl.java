package com.kitty.mallmng.service.test.impl;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.alibaba.csp.sentinel.slots.block.flow.FlowException;
import com.alibaba.fastjson.JSON;
import com.alibaba.nacos.api.config.annotation.NacosValue;
import com.kitty.mallmng.data.model.UserDO;
import com.kitty.mallmng.service.test.TestService;
import com.kitty.mallmng.service.test.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("testService")
public class TestServiceImpl implements TestService {

    private static final Logger logger = LoggerFactory.getLogger(TestServiceImpl.class);

    private String dataUrl;

    @Autowired
    private UserService service;


    @Override
    @SentinelResource(value = "test_service")
    public String test() {
        logger.info("TestServiceImpl:{}",dataUrl);
        List<UserDO> list = service.list();
        logger.info("list:{}", JSON.toJSONString(list));
        return "test";
    }

    @Override
    @SentinelResource(value = "query_list",fallback = "queryListFallback")
    public String queryList(String name) {
            return "queryList:"+name;
    }

    private String queryListFallback(String name, FlowException flowException) {
        return "error:"+name;
    }
}
