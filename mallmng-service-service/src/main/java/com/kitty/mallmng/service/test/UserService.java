package com.kitty.mallmng.service.test;

import com.baomidou.mybatisplus.extension.service.IService;
import com.kitty.mallmng.data.model.UserDO;

public interface UserService extends IService<UserDO> {
}
