package com.kitty.mallmng.service.test.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.kitty.mallmng.data.mapper.UserMapper;
import com.kitty.mallmng.data.model.UserDO;
import com.kitty.mallmng.service.test.UserService;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, UserDO> implements UserService {
}
