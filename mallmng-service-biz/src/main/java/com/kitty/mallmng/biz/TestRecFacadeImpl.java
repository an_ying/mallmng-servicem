package com.kitty.mallmng.biz;

import com.kitty.mallmng.data.model.UserDO;
import com.kitty.mallmng.facade.TestRecFacade;
import com.kitty.mallmng.facade.vo.TestReq;
import com.kitty.mallmng.facade.vo.TestResp;
import com.kitty.mallmng.service.test.UserService;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

@DubboService(version = "1.0.0")
public class TestRecFacadeImpl implements TestRecFacade {

    @Value("${demo.service.name}")
    private String serviceName;

    @Autowired
    private UserService userService;

    @Override
    public TestResp testRpc(TestReq req) {
        TestResp testResp = new TestResp();
        testResp.setUserName(serviceName);

        UserDO userDO = new UserDO();
        userDO.setId(2L);
        userDO.setAccount("7777");
        userService.updateById(userDO);
        return testResp;
    }
}
