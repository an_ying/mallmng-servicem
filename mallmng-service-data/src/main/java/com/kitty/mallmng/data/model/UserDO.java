package com.kitty.mallmng.data.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

@TableName("account")
@Data
@Accessors(chain = true)
public class UserDO implements Serializable {

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    private String account;
}
