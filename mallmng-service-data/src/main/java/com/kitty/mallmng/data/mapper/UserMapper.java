package com.kitty.mallmng.data.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.kitty.mallmng.data.model.UserDO;


public interface UserMapper extends BaseMapper<UserDO> {
}
