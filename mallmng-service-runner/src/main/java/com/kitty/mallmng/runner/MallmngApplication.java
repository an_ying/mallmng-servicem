package com.kitty.mallmng.runner;


import com.alibaba.nacos.spring.context.annotation.config.NacosPropertySource;
import org.apache.dubbo.config.spring.context.annotation.EnableDubbo;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication(scanBasePackages = "com.kitty.mallmng",exclude = DataSourceAutoConfiguration.class)
@MapperScan("com.kitty.mallmng.data.mapper")
@EnableTransactionManagement
@EnableDubbo(scanBasePackages = "com.kitty.mallmng")
@PropertySource("classpath:dubbo-config.properties")
@NacosPropertySource(dataId = "query_page_size",groupId = "DEFAULT_GROUP",autoRefreshed = true)
public class MallmngApplication {

    public static void main(String[] args) {
        SpringApplication.run(MallmngApplication.class,args);
    }

}
