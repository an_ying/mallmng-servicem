package com.kitty.mallmng.runner;

import io.seata.spring.annotation.GlobalTransactionScanner;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Description:
 * Datetime:    2024/2/4   14:37
 * Author:   kete
 */
@Configuration
public class SeataDataConfig {

    @Value("${spring.application.name}")
    private String applicationId;


    @Bean
    public GlobalTransactionScanner getGlobalTransactionScanner()
    {
        return new GlobalTransactionScanner(applicationId,"my_test_tx_group");
    }
}
