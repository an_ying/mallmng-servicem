package com.kitty.mallmng.web.controller;

import com.kitty.mallmng.service.test.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {

    @Autowired
    private TestService testService;

    @RequestMapping("/test")
    @ResponseBody
    public String test(){
        return testService.test();
    }

    @RequestMapping("/queryList")
    public String queryList(String name){
        return testService.queryList(name);
    }
}
